/******************************************************************************
* Copyright (c) 2000-2023 Ericsson AB
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
*
* Contributors:
*   Gabor Szalai - initial implementation and initial documentation
******************************************************************************/
//
//  File:               TS29503_Nudm_EE.ttcn
//  Description:	      Type definitions for 3GPP TS29503
/////////////////////////////////////////////// 
module TS29503_Nudm_EE {

  import from TS29571_CommonData all

external function f_enc_EeSubscription(in EeSubscription pdu) return octetstring 
with { extension "prototype(convert) encode(JSON)" }

external function f_dec_EeSubscription(in octetstring stream, out EeSubscription pdu) return integer 
with { extension "prototype(backtrack) decode(JSON)" }

external function f_enc_CreatedEeSubscription(in CreatedEeSubscription pdu) return octetstring 
with { extension "prototype(convert) encode(JSON)" }

external function f_dec_CreatedEeSubscription(in octetstring stream, out CreatedEeSubscription pdu) return integer 
with { extension "prototype(backtrack) decode(JSON)" }


  type set CreatedEeSubscription {
    EeSubscription    eeSubscription,
    TS29571_CommonData.Uinteger    numberOfUes optional,
    set of       MonitoringReport    eventReports optional
  }

  type set EeSubscription {
    TS29571_CommonData.Uri    callbackReference,
    set of record {
      universal charstring key,
      MonitoringConfiguration    additionalProperties
    } monitoringConfiguration,
    ReportingOptions    reportingOptions optional,
    TS29571_CommonData.SupportedFeatures    supportedFeatures optional
  } with {
    variant (monitoringConfiguration) "as map"
  }

  type set MonitoringConfiguration {
    ReferenceId    referenceId,
    EventType    eventType,
    boolean immediateFlag optional
  }

  type set ReportingOptions {
    MaxNumOfReports    maxNumOfReports optional,
    TS29571_CommonData.DateTime    monitoringDuration optional
  }

  type set MonitoringReport {
    ReferenceId    referenceId optional,
    EventType    eventType,
    Report    report optional,
    TS29571_CommonData.Gpsi    gpsi optional,
    TS29571_CommonData.DateTime    timeStamp
  }

  type union Report {
    ChangeOfSupiPeiAssociationReport    changeOfSupiPeiAssociationReport,
    RoamingStatusReport    roamingStatusReport
  } with {
    variant "JSON: as value"
  }


  type set ChangeOfSupiPeiAssociationReport {
    TS29571_CommonData.Pei    newPei
  }

  type set RoamingStatusReport {
    boolean roaming,
    TS29571_CommonData.PlmnId    newServingPlmn
  }

  type integer ReferenceId

  type integer MaxNumOfReports

  type enumerated EventType_enum { LOSS_OF_CONNECTIVITY, UE_REACHABILITY_FOR_DATA, UE_REACHABILITY_FOR_SMS, LOCATION_REPORTING, CHANGE_OF_SUPI_PEI_ASSOCIATION, ROAMING_STATUS, COMMUNICATION_FAILURE, AVAILABILITY_AFTER_DNN_FAILURE}

  type union EventType {
    EventType_enum  enum_val,
    charstring           other_val
  } with {
    variant "JSON: as value"
  }






} with {
  encode "JSON"
}
